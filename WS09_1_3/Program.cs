﻿using System;

namespace WS09_1_3
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * true \ false - истина\ложь (bool)
             * -10, 10, 20, 10000 - целочисленные (int)
             * 0b11, 0b10101 - целочисленные выраженные в двоичном формате 0b... (int)
             * 0x0A, 0x0F - целочисленные вараженные в 16чном формате 0x... (int)
             * 3.14, 3.15, -3.14 - вещественные двойной точности (double)
             * 3.14f, 3.15f, -3.14f - вещественные (float)
             * 'G', 'r', 's', 'u', '\n', '\t' - символы (char)
             * "GRSU\n2020", "l33t = 1337" - строки (string)
             * null - ссылка в никакое значение
             */

            bool isTrue = true, isFalse = false;

            Console.WriteLine($"{isTrue}, {isFalse}");
            Console.ReadKey();
            Console.Clear();

            int isInt1 = -10,   isInt2 = 10, isInt3 = 20, isInt4 = 10000;
            int isIntB1 = 0b11, isIntB2 = 0b10101;
            int isIntH1 = 0x0A, isIntH2 = 0x0F;

            Console.WriteLine($"{isInt1}, {isInt2}, {isInt3}, {isInt4}");
            Console.WriteLine($"{isIntB1}, {isIntB2}");
            Console.WriteLine($"{isIntH1}, {isIntH2}");
            Console.ReadKey();
            Console.Clear();

            double isDouble1 = 3.14,     isDouble2 = 3.15, isDouble3 = -3.14;
            float  isFloat1 = 3.14f,     isFloat2 = 3.15f, isFloat3 = -3.14f;

            Console.WriteLine($"{isDouble1}, {isDouble2}, {isDouble3}");
            Console.WriteLine($"{isFloat1}, {isFloat2}, {isFloat3}");
            Console.ReadKey();
            Console.Clear();

            char 
                isChar1 = 'G', 
                isChar2 = 'r', 
                isChar3 = 's', 
                isChar4 = 'u', 
                isChar5 = '\n', 
                isChar6 = '\t';

            Console.WriteLine($"{isChar1}, {isChar2}, {isChar3}, {isChar4}, {isChar5}, {isChar6}");
            Console.ReadKey();
            Console.Clear();

            string isString1 = "GRSU\n2020", isString2 = "l33t = 1337";

            Console.WriteLine($"{isString1}, {isString2}");

            //Console.WriteLine(null); - null нет Console.WriteLine (by default)
        }
    }
}
