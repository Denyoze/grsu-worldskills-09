﻿using System;

namespace WS09_1_7
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * a == b           - равно ли значение a значению b
             * a != b           - значение a, не равно b (проверка неравенства значений)
             * a < b, a > b     - значение a меньше\больше значению b
             * a <= b, a >= b   - значение a меньше и равно\ больше и равно значению b
             */

            int a = 10, b = 20;

            Console.WriteLine($"{a} == {b}? -> {a == b}");
            Console.WriteLine($"{a} != {b}? -> {a != b}");

            Console.WriteLine($"{a} < {b}? -> {a < b}");
            Console.WriteLine($"{a} > {b}? -> {a > b}");

            Console.WriteLine($"{a} <= {b}? -> {a <= b}");
            Console.WriteLine($"{a} >= {b}? -> {a >= b}");

            Console.ReadKey();
            Console.Clear();

            /*
             * !a       - отрицание a (если a = true, !a = false | если a = false, !a = true)
             * a && b   - a И b = true? (если a И b = true)
             * a || b   - a ИЛИ b = true? (если a ИЛИ b = true)
             */

            bool bPositive = true, bNegative = false;

            Console.WriteLine($"!{bPositive} = {!bPositive}");
            Console.WriteLine($"!{bNegative} = {!bNegative}");

            Console.WriteLine($"{bPositive} && {bNegative} = {bPositive && bNegative}");
            Console.WriteLine($"{bPositive} || {bNegative} = {bPositive || bNegative}");

            Console.ReadKey();
            Console.Clear();

            Console.WriteLine($"!true && (true || false) = {!true && (true || false)}"); // можно комбинировать, как и любые другие операции
            Console.WriteLine($"-> (1) !true = {!true}");
            Console.WriteLine($"-> (2) true || false = {true || false}");
            Console.WriteLine($"-> (3) false && true = {false && true}");

            Console.ReadKey();
            Console.Clear();

            /* 
             * "True" != true, "False" != false
             * False в консоли  - преобразованная в строку переменная типа bool!
             * True в консоли   - преобразованная в строку переменная типа bool!
             */

            Console.WriteLine($"true == \"True\" -> {true.ToString() == "True"}"); // true.ToString() - вызывается автоматически, при преобразовании в строку любого типа

            // Напрямую true != "True", в строке (если true преобразована в string) true == "True" (если иное не определено)
            // Преобразование базовых типов
        }
    }
}
