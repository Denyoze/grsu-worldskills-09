﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WS_09_2_5
{
    class Program
    {
        static void GetSum(out int sum, params int[] arr)
        {
            sum = 0;

            foreach(var val in arr)
            {
                sum += val;
            }
        }

        static int GetLength(params object[] arr) => arr.Length;

        static void Main(string[] args)
        {
            int sum = 0;
            GetSum(out sum, 1, 1, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

            Console.WriteLine($"Sum = {sum}");
            Console.WriteLine($"My test func length = {GetLength(1, 2, 3, 4, 5, 10)}");
        }
    }
}
