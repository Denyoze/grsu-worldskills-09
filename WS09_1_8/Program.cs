﻿using System;
using System.Linq;

namespace WS09_1_8
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 10, b = 20;

            /* if - если
             * else - иначе
             * else if - смесь иначе + если
             * 
             * else if идёт до else!
             */

            if (a > b)
            {
                Console.WriteLine("a > b");
            }
            else if (a == 30)
            {
                Console.WriteLine("Impossible!");
            }
            else
            {
                Console.WriteLine("a <= b");
            }

            // Смешаем

            if (a > b || true)
            {
                Console.WriteLine("Бум");
            }

            /*
             * Можно писать так:
             * if (statement) {
             *  ...
             * } else {
             *  ...
             * }
             * 
             * или так:
             * 
             * if (statement)
             * {
             *  ...
             * } 
             * else
             * {
             *  ...
             * }
             */

            Console.ReadKey();
            Console.Clear();

            // switch/case - перебирает значение (заменяет множественные if)
            // break - прерывает
            // goto case - переходит к case (например: goto case 20 - перейти к case 20:)

            switch (a)
            {
                case 10:
                    Console.WriteLine("10!");
                    break;
                case 20:
                    Console.WriteLine("20!!");
                    break;
                case 30:
                    Console.WriteLine("30!!!");
                    break;
                default:
                    Console.WriteLine("Dunno, any value?");
                    break;
            }

            Console.ReadKey();
            Console.Clear();

            // Тернарная операция
            /*
             * (a) ? (b) : (c)
             * Вернет НАЛЕВО b, если a == true, иначе вернет c
             */

            int 
                result1 = (true) ? (123) : (321),
                result2 = (false) ? (123) : (321);

            Console.WriteLine($"result1 = {result1}");
            Console.WriteLine($"result2 = {result2}");
        }
    }
}
