﻿using System;

namespace WS09_2_1
{
    class Program
    {
        static void Main(string[] args)
        {
            #region FOR
            Console.WriteLine("-- For --");

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"Current (i) value = {i}");
            }

            Console.ReadKey();
            Console.WriteLine("-- For (without \"body\") --");

            int a = 0;

            for (; a < 10; )
            {
                Console.WriteLine($"Current (a) value = {a}");
                a++;
            }

            Console.ReadKey();
            #endregion

            Console.Clear();

            #region DO\WHILE
            Console.WriteLine("-- Do\\While --");

            int doWhileCounter = 5;
            do
            {
                Console.WriteLine($"Current (doWhileCounter) = {doWhileCounter}");
                doWhileCounter--;
            } while (doWhileCounter > 0);

            Console.ReadKey();
            Console.WriteLine("-- Do\\While (with false) --");

            const bool constValueFalse = false;

            do // do...
            {
                Console.WriteLine("Hello!");
            } while (constValueFalse); // if true, do one more time...

            Console.ReadKey();
            #endregion


            Console.Clear();

            #region WHILE
            Console.WriteLine("-- While --");

            int whileCounter = 10;

            while (whileCounter > 0) // if true, do...
            {
                Console.WriteLine($"Current (whileCounter) value = {whileCounter}");
                whileCounter--;
            }

            Console.ReadKey();
            Console.WriteLine("-- While + break + continue --");

            string secretCode = "";

            while(true)
            {
                Console.Write("Password: ");
                secretCode = Console.ReadLine();

                if (secretCode == "secret")
                {
                    break;
                }

                Console.WriteLine("- Wrong password!");

                if (secretCode == "con")
                {
                    continue;
                }

                Console.Clear();
                Console.WriteLine("- Try again!");
            }
            Console.WriteLine("After while");

            Console.ReadKey();
            #endregion
        }
    }
}
