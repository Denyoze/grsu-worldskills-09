﻿// Однострочный комментарий как в C/C++

/*
 * Комментарий 
 * на 
 * несколько 
 * строк
 * как в C/C++
 */

using System;                        // Очень нужное пространство имён
using System.Collections.Generic;    // Очень не нужное (сейчас) пространство имён
using System.Linq;                   // Очень не нужное (сейчас) пространство имён
using System.Text;                   // Очень не нужное (сейчас) пространство имён
using System.Threading.Tasks;        // Очень не нужное (сейчас) пространство имён

namespace WS09_1_1 // Пространство имён
{
    class Program // Класс
    {
        static void Main(string[] args) // Метод, static = статичный
        {
            Console.Write("Введите имя: "); // std::cout << "Введите имя: ";
            string name = Console.ReadLine(); // Инициализация переменной строкой из консольного ввода

            Console.WriteLine($"Hello, {name}!"); // "" <- строка, $"{varName}" <- строка, но с интерполяцией
        }
    }
}
