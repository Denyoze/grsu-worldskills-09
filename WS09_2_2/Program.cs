﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WS09_2_2
{
    class Program
    {
        static void Main(string[] args)
        {
            #region ARRAY
            /*
             * type[] name - array;
             * int[] values = new int[4]; 4 = size of an array
             */

            int[]
                emptyArr = new int[10],
                values = new int[10] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                valuesWithAutoSize = new int[] { 0, 2, 4, 6, 8 },
                valuesLazy = { 1, 3, 5, 7, 9 };

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"-> emptyArr[{i}] = {emptyArr[i]}");
            }

            Console.ReadKey();
            Console.WriteLine();
            
            for (int i = 0; i < values.Length; i++) // <array>.Length - размер массива
            {
                Console.WriteLine($"-> values[{i}] = {values[i]}");
            }

            Console.ReadKey();
            Console.WriteLine();

            // Начинаем не с 0, а с 1
            valuesWithAutoSize[0] = 1337;

            foreach(int value in valuesWithAutoSize)
            {
                Console.WriteLine($"-> valuesWithAutoSize[?] = {value}");
            }

            Console.ReadKey();
            Console.WriteLine();

            foreach (int value in valuesLazy)
            {
                Console.WriteLine($"-> valuesLazy[?] = {value}");
            }

            Console.ReadKey();
            #endregion

            Console.Clear();

            #region MULT. DIM. ARRAY

            int[,] arr = { 
                { 1, 0, 4 }, 
                { 3, 5, 7 }
            };

            // arr[column, row] //

            Console.WriteLine($"Rank = {arr.Rank}");
            Console.WriteLine($"{arr[0, 0]}{arr[1, 0]}{arr[1, 0]}{arr[1, 2]}");

            Console.ReadKey();
            #endregion

            Console.Clear();

            #region ARR OF ARR

            int[][] arrOfArr = new int[2][]
            {
                new int[3] { 1, 2, 3 },
                new int[4] { 4, 5, 6, 7 }
            };

            Console.WriteLine($"[arrOfArr] length = {arrOfArr.Length}");

            foreach (var rowArr in arrOfArr)
            {
                Console.WriteLine($"-> Rank = {rowArr.Rank}, length = {rowArr.Length}");

                foreach (var element in rowArr)
                {
                    Console.Write($"{element}\t");
                }
                Console.WriteLine();
            }

            Console.ReadKey();
            #endregion
        }
    }
}
