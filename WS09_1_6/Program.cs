﻿using System;

namespace WS09_1_6
{
    class Program
    {
        static void Main(string[] args)
        {
            int superNumebr = 10;
            Console.WriteLine($"superNumber = {superNumebr}");

            Console.ReadKey();
            Console.Clear();

            int a, b, c;
            a = b = c = 1337; // a = 1337, b = 1337, c = 1337

            Console.WriteLine($"a = {a}, b = {b}, c = {c}");

            Console.ReadKey();
            Console.Clear();

            /*
             * (a += b) - (a = a + b) 
             * (a -= b) - (a = a - b)
             * (a *= b) - (a = a * b)
             * (a /= b) - (a = a / b)
             * (a %= b) - (a = a % b)
             */

            int intForTesting = 10;
            Console.WriteLine($"intForTesting = {intForTesting}");

            Console.ReadKey();
            Console.Clear();

            Console.WriteLine($"(intForTesting += 10) = {intForTesting += 10}");
            Console.WriteLine($"intForTesting = {intForTesting}");

            Console.ReadKey();
            Console.Clear();

            Console.WriteLine($"(intForTesting -= 10) = {intForTesting -= 10}");
            Console.WriteLine($"intForTesting = {intForTesting}");
        }
    }
}
