﻿using System;

namespace WS09_1_5
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * a + b (сложение)
             * a - b (вычитание)
             * a * b (умножение)
             * a / b (деление)
             * a % b (остаток от деления чисел)
             */

            int a = 10, b = 10;

            double bDouble = 2.3;
            float bFloat = 2.3f;

            Console.WriteLine($"{a} + {b} = {a + b}"); // сложение (int)
            Console.WriteLine($"{a} - {b} = {a - b}"); // вычитание (int)
            Console.WriteLine($"{a} * {b} = {a * b}"); // умножение (int)
            Console.WriteLine($"{a} / {b} = {a / b}"); // деление (int)
            Console.WriteLine($"{a} / {bFloat} = {a / bFloat}"); // деление (float)
            Console.WriteLine($"{a} / {bDouble} = {a / bDouble}"); // деление (double)

            Console.ReadKey();
            Console.Clear();

            /*
             * При операции с разными типами чисел, результат (по типу) = самому жирному типу 
             * (int + float = float)
             * (float + double = double)
             */

            Console.WriteLine($"{a} % 2 = {a % 2}"); // остаток от деления (int)
            Console.WriteLine($"{a} % 2.7 = {a % 2.7}"); // остаток от деления (double)
            Console.WriteLine($"{a} % 2.7f = {a % 2.7f}"); // остаток от деления (float)

            Console.ReadKey();
            Console.Clear();

            /*
             * a++ - постфиксный инкремент  (вернёт значение, а потому увеличит)
             * ++a - префиксный  инкримент  (увеличит значение, а потом вернёт)
             * 
             * a-- - постфиксный декремент  (вернёт значение, а потому уменьшит)
             * --a - префиксный  декремент  (уменьшит значение, а потом вернёт)
             */

            int testInc = 10;
            Console.WriteLine($"testInc++ = {testInc++}");
            Console.WriteLine($"testInc = {testInc}");

            Console.WriteLine($"++testInc = {++testInc}");
            Console.WriteLine($"testInc = {testInc}");

            Console.ReadKey();
            Console.Clear();

            int testDec = 10;
            Console.WriteLine($"testDec-- = {testDec--}");
            Console.WriteLine($"testDec = {testDec}");

            Console.WriteLine($"--testDec = {--testDec}");
            Console.WriteLine($"testDec = {testDec}");
        }
    }
}
