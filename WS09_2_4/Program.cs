﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WS09_2_4
{
    class Program
    {
        static void WrongIncVal(int val) => val++;
        static void IncVal(ref int val) => val++;
        static void MultiTool(int val1, int val2, out int sum, out int mult)
        {
            sum = val1 + val2;
            mult = val1 * val2;
        }
        static void MultiToolWithIn(in int val1, in int val2, out int sum, out int mult)
        {
            //val1 += 10;
            MultiTool(val1, val2, out sum, out mult);
        }

        static void Main(string[] args)
        {
            int a = 10;
            Console.WriteLine($"a = {a}");
            Console.ReadKey();

            WrongIncVal(a);
            Console.WriteLine($"> a = {a}");
            Console.ReadKey();

            IncVal(ref a);
            Console.WriteLine($"> a = {a}");
            Console.ReadKey();

            int b = 97, sum = 0, mult = 0;
            Console.WriteLine($"a = {a}, b = {b}");

            MultiTool(a, b, out sum, out mult);
            Console.WriteLine($"> a = {a}, b = {b}\nsum = {sum}, mult = {mult}");
            Console.ReadKey();

            MultiToolWithIn(a, b, out sum, out mult);
            Console.WriteLine($"> a = {a}, b = {b}\nsum = {sum}, mult = {mult}");
            Console.ReadKey();

        }
    }
}
