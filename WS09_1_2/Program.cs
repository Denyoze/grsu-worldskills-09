﻿using System;

namespace WS09_1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            // тип имя (= значение);

            string name; // определяем
            name = "aaaaaaaa"; // присвоили значение

            string NaMe = "AAAAAAAA"; // определили + присвоили = инициализировали

            var varName = "aAaAaA VAR?"; // как auto в C++, определяет тип переменной по значению

            /*
             * name != NaMe
             * в C# важен регистр у переменных (регистрозависимый язык)
             */

            Console.WriteLine(name);
            Console.WriteLine(NaMe);
            Console.WriteLine(varName);
        }
    }
}
