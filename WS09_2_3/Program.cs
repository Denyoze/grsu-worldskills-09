﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WS09_2_3
{
    class Program
    {
        static void PrintArr(int[] arr)
        {
            for (var index = 0; index < arr.Length; index++)
            {
                Console.WriteLine($"::[{index}] = {arr[index]}");
            }
        }

        static int SumArr(int[] arr, bool displayTemp = false)
        {
            int sum = 0;

            foreach(var value in arr)
            {
                if (displayTemp)
                {
                    Console.WriteLine($":: sum = {sum}, sum += {value}");
                }

                sum += value;
            }

            return sum;
            Console.WriteLine(":(");
        }

        static int[] GetEmptyArray(int size) => new int[size];

        static void Main(string[] args)
        {
            int[] myArr = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            PrintArr(myArr);

            Console.WriteLine($"Sum = {SumArr(myArr)}");
            Console.ReadKey();
            Console.WriteLine($"Sum = {SumArr(myArr, true)}");

            Console.ReadKey();
            Console.Clear();

            Console.Write("Size?: ");
            int size = int.Parse(Console.ReadLine());

            int[] newArr = new int[size];
            Console.WriteLine($"new Arr length = {newArr.Length}");
            PrintArr(newArr);

            for(var index = 0; index < newArr.Length; index++)
            {
                Console.Write($"newArr[{index}] = ");
                newArr[index] = int.Parse(Console.ReadLine());
            }

            PrintArr(newArr);

            Console.ReadKey();
            Console.Clear();

            Console.WriteLine($"Sum = {SumArr(displayTemp: true, arr: newArr)}");
        }
    }
}
